﻿namespace DemoASPAude.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHobbyToContact : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contact",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 100, unicode: false),
                        Nom = c.String(nullable: false, maxLength: 50),
                        Prenom = c.String(nullable: false, maxLength: 50),
                        DateDeNaissance = c.DateTime(nullable: false, storeType: "date"),
                        Email = c.String(maxLength: 255, unicode: false),
                        Favori = c.Boolean(nullable: false),
                        Hobby = c.String(maxLength: 100),
                        UserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserEntities", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 255),
                        Password = c.Binary(nullable: false, maxLength: 8000),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contact", "UserId", "dbo.UserEntities");
            DropIndex("dbo.Contact", new[] { "UserId" });
            DropTable("dbo.UserEntities");
            DropTable("dbo.Contact");
        }
    }
}

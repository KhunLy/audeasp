﻿using DemoASPAude.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoASPAude.DAL.Repositories
{
    public class ContactRepository
    {
        private DemoContext _context;

        public ContactRepository()
        {
            _context = new DemoContext("name=default");
        }

        public IEnumerable<ContactEntity> GetAll()
        {
            return _context.Contacts.Include("User");
        }

        public ContactEntity Get(string id)
        {
            return _context.Contacts.Find(id);
        }

        public IEnumerable<ContactEntity> GetFavorite()
        {
            return _context.Contacts
                .Where(c => c.Favori);
        }

        public string Add(ContactEntity c)
        {
            ContactEntity newContact = _context.Contacts.Add(c);
            _context.SaveChanges();
            return newContact.Id;
        }

        public void AddToFavorite(ContactEntity c)
        {
            c.Favori = true;
            _context.SaveChanges();
        }

        public void Delete(ContactEntity c)
        {
            _context.Contacts.Remove(c);
            _context.SaveChanges();
        }
    }
}

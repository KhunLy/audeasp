﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoASPAude.Models
{
    public class Contact
    {
        public string Id { get; set; } 
        public string Nom { get; set; }
        public string Prenom {  get; set; }
        public DateTime DateDeNaissance { get; set; }
        public string Email { get; set; }
        public bool Favori { get; set; }
    }
}
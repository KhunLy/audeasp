﻿using DemoASPAude.DAL.Repositories;
using DemoASPAude.DAL.Entities;
using DemoASPAude.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoASPAude.Controllers
{
    public class ContactController : Controller
    {
        private ContactRepository contactRepository;

        public ContactController()
        {
            contactRepository = new ContactRepository();
        }

        // GET: Contact
        public ActionResult Index()
        {
            return View(contactRepository.GetAll().Select(c => new Contact
            {
                Id = c.Id,
                DateDeNaissance= c.DateDeNaissance,
                Nom = c.Nom,
                Prenom = c.Prenom,
                Email = c.Email,
                Favori = c.Favori,
            }).ToList());
        }

        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(CreateContactForm form)
        {
            if(ModelState.IsValid)
            {
                ContactEntity contact = new ContactEntity();
                contact.Id = Guid.NewGuid().ToString();
                contact.Nom = form.Nom;
                contact.Prenom = form.Prenom;
                contact.Email = form.Email;
                contact.DateDeNaissance = form.DateDeNaissance;
                contact.Favori = false;
                contactRepository.Add(contact);
                TempData["success"] = "Enregistrement OK";
                return RedirectToAction("Index");
            }
            TempData["error"] = "Enregistrement KO";
            return View(form);
        }

        public ActionResult Delete(string id)
        {
            ContactEntity contact = contactRepository.Get(id);
            if (contact is null)
            {
                return new HttpNotFoundResult();
            }
            contactRepository.Delete(contact);
            return RedirectToAction("Index");
        }

        public ActionResult AddToFavorite(string id)
        {
            ContactEntity contact = contactRepository.Get(id);
            if (contact is null)
            {
                return new HttpNotFoundResult();
            }
            contactRepository.AddToFavorite(contact);
            return RedirectToAction("Favorite");
        }

        public ActionResult Favorite()
        {
            return View(contactRepository.GetFavorite().Select(c => new Contact
            {
                Id = c.Id,
                DateDeNaissance = c.DateDeNaissance,
                Nom = c.Nom,
                Prenom = c.Prenom,
                Email = c.Email,
                Favori = c.Favori,
            }).ToList());
        }
    }
}
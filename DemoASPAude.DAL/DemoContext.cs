﻿using DemoASPAude.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoASPAude.DAL
{
    public class DemoContext: DbContext
    {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ContactEntity> Contacts { get; set; }

        public DemoContext()
            : base("server=K-PC;database=demoAude;integrated security=true")
        {

        }

        public DemoContext(string connectionString)
            : base(connectionString)
        {
            // Database.SetInitializer(new CreateDatabaseIfNotExists<DemoContext>());
        }

    }
}

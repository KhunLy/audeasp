﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoASPAude.Models
{
    public class CreateContactForm
    {
        [Required]
        public string Nom { get; set; }
        [Required]
        public string Prenom { get; set; }
        public DateTime DateDeNaissance { get; set; }
        [EmailAddress]
        public string Email { get; set; }
    }
}
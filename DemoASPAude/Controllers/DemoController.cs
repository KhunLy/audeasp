﻿using DemoASPAude.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace DemoASPAude.Controllers
{
    public class DemoController : Controller
    {
        // GET: Demo
        public ActionResult Index(int nbDepart)
        {
            return View(nbDepart);
        }

        public ActionResult CalculBMI()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CalculBMI(BmiForm form)
        {
            return View(new BmiModel { 
                Poids = form.Poids,
                Taille = form.Taille,
            });
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactForm form)
        {
            if(ModelState.IsValid) // verifier la validité du formulaire
            {
                #region Config pour l'envoi
                SmtpClient client = new SmtpClient(); // service permettant l'envoi d'email
                client.Host = "smtp.gmail.com"; // smtp de gmail
                client.Port = 587; // port de connection au serveur
                client.EnableSsl = true; // encrytion de sdonnées
                client.Credentials = new NetworkCredential("testaudekhun@gmail.com", "test1234=");
                #endregion

                #region création du mail
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("lykhun@gmail.com");
                mail.To.Add(new MailAddress("lykhun@gmail.com"));
                mail.Subject = "Test";
                mail.IsBodyHtml = true;
                mail.Body = "Vous avez recu un message de la part de " + form.Email;
                mail.Body += "Contenu :" + form.Contenu; 
                #endregion
                client.Send(mail);
                return RedirectToAction("Index", "Home");
            }
            return View(form);
        }
    }
}
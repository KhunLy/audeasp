﻿namespace DemoASPAude.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveHobby : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Contact", "Hobby");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contact", "Hobby", c => c.String(maxLength: 100));
        }
    }
}

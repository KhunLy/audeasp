﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoASPAude.DAL.Entities
{
    public class UserEntity
    {
        [Key]
        [Column("Id", TypeName = "INT")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("UserName", TypeName = "NVARCHAR")]
        [MaxLength(255)]
        [Required]
        public string UserName { get; set; }

        [Column("Password", TypeName = "VARBINARY")]
        [Required]
        public byte[] Password { get; set; }

        public ICollection<ContactEntity> Contacts { get; set; }
    }
}

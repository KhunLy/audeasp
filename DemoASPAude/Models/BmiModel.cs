﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoASPAude.Models
{
    public class BmiModel
    {
        public int Poids { get; set; }
        public int Taille { get; set; }
        public double Bmi { 
            get {
                return Poids / ((Taille / 100F) * (Taille / 100F));
            } 
        }
    }
}
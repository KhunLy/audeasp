﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoASPAude.DAL.Entities
{
    [Table("Contact")]
    public class ContactEntity
    {
        [Key]
        [Column("Id", TypeName = "VARCHAR")]
        [MaxLength(100)]
        public string Id { get; set; }

        [Column("Nom", TypeName = "NVARCHAR")]
        [MaxLength(50)]
        [Required]
        public string Nom { get; set; }

        [Column("Prenom", TypeName = "NVARCHAR")]
        [MaxLength(50)]
        [Required]
        public string Prenom { get; set; }

        [Column("DateDeNaissance", TypeName = "DATE")]
        public DateTime DateDeNaissance { get; set; }

        [Column("Email", TypeName = "VARCHAR")]
        [MaxLength(255)]
        public string Email { get; set; }

        [Column("Favori", TypeName = "BIT")]
        public bool Favori { get; set; }


        [Column("UserId", TypeName = "INT")]
        [ForeignKey("User")]
        public int? UserId { get; set; }

        public UserEntity User { get; set; }
    }
}
